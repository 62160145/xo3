/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.aomsinjook.xo3.Player;
import com.aomsinjook.xo3.Table;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author cherz__n
 */
public class TestOX {
    
    public TestOX() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }
    public void testRow1(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x,o);
        table.setRowCol(0,0);
        table.setRowCol(0,1);
        table.setRowCol(0,2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(x, table.getWinner());
    }
     public void testRow2(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x,o);
        table.setRowCol(1,0);
        table.setRowCol(1,1);
        table.setRowCol(1,2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(x, table.getWinner());

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
}
     public void testRow3(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x,o);
        table.setRowCol(2,0);
        table.setRowCol(2,1);
        table.setRowCol(2,2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(x, table.getWinner());
    }
     public void testcolumn1(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x,o);
        table.setRowCol(0,0);
        table.setRowCol(1,0);
        table.setRowCol(2,0);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(x, table.getWinner());
     
}
     public void testcolumn2(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x,o);
        table.setRowCol(0,1);
        table.setRowCol(1,1);
        table.setRowCol(2,1);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(x, table.getWinner());
     
}
     public void testcolumn3(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x,o);
        table.setRowCol(0,2);
        table.setRowCol(1,2);
        table.setRowCol(2,2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(x, table.getWinner());
     
}
     public void testx1(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x,o);
        table.setRowCol(0,0);
        table.setRowCol(1,1);
        table.setRowCol(2,2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(x, table.getWinner());
     
}
     public void testx2(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x,o);
        table.setRowCol(0,2);
        table.setRowCol(1,1);
        table.setRowCol(2,0);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(x, table.getWinner());
     
}
     public void testRow1O(){
        Player o = new Player('O');
        Player x = new Player('X');
        Table table = new Table(o,x);
        table.setRowCol(0,0);
        table.setRowCol(0,1);
        table.setRowCol(0,2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(o, table.getWinner());
    }
     public void testRow2O(){
        Player o = new Player('O');
        Player x = new Player('X');
        Table table = new Table(o,x);
        table.setRowCol(1,0);
        table.setRowCol(1,1);
        table.setRowCol(1,2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(o, table.getWinner());

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
}
     public void testRow3O(){
        Player o = new Player('O');
        Player x = new Player('X');
        Table table = new Table(o,x);
        table.setRowCol(2,0);
        table.setRowCol(2,1);
        table.setRowCol(2,2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(o, table.getWinner());
    }
     public void testcolumn1O(){
        Player o = new Player('O');
        Player x = new Player('X');
        Table table = new Table(o,x);
        table.setRowCol(0,0);
        table.setRowCol(1,0);
        table.setRowCol(2,0);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(o, table.getWinner());
     
}
     public void testcolumn2O(){
        Player o = new Player('O');
        Player x = new Player('x');
        Table table = new Table(o,x);
        table.setRowCol(0,1);
        table.setRowCol(1,1);
        table.setRowCol(2,1);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(o, table.getWinner());
     
}
     public void testcolumn3O(){
        Player o = new Player('O');
        Player x = new Player('X');
        Table table = new Table(o,x);
        table.setRowCol(0,2);
        table.setRowCol(1,2);
        table.setRowCol(2,2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(o, table.getWinner());
     
}
     public void testx1O(){
        Player o = new Player('O');
        Player x = new Player('X');
        Table table = new Table(o,x);
        table.setRowCol(0,0);
        table.setRowCol(1,1);
        table.setRowCol(2,2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(o, table.getWinner());
     
}
     public void testx2O(){
        Player o = new Player('O');
        Player x = new Player('X');
        Table table = new Table(o,x);
        table.setRowCol(0,2);
        table.setRowCol(1,1);
        table.setRowCol(2,0);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(o, table.getWinner());
     
}
     
}
